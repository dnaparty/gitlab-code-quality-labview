# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2020-12-04
### Added
- feat: Unit Tests for most of the features.
- feat: Fingerprint for each of the test failures.
- feat: Included Code Severity according to Gitlab 13.6 update.
- feat: Gitlab CI and CI-CD Tools as SDM Dependency.

### Changed
- fix: The Library name. Now the library is dedicated only to Code Quality.

## [0.1.0] - 09-20-2020
### Added
- Initial Version with Code Quality Report.
