# Gilab Code Quality Report Tool

This project has a set of VIs for parsing the LabVIEW VI Analyzer Result Cluster into Gitlab Code Quality JSON Format.

# Author

Felipe Pinheiro Silva

## Installation

Use this library through git submodules or SDM.

### Compatibility

LabVIEW 2020

- Older Versions in [Releases](../../../-/releases/)

## Usage
### Code Quality

Insert this code at the end of the VI Analyzer Task and redirect the results cluster to this the Generate Code Quality VI.

![Simple Usage](/docs/snippets/usage_code_quality.png)


## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[BSD3](LICENSE)
